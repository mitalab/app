import React, { useEffect } from 'react';
import store from './src/services/libs/store';
import { Provider } from 'react-redux'
import Navigation from './navigation'
import SplashScreen from 'react-native-splash-screen';
import { enableScreens } from 'react-native-screens';
enableScreens(false);
export default function App() {
  useEffect(() => {
    SplashScreen.hide();
  }, [])
  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
}
