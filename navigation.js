import React, { useState, useEffect } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector, useDispatch } from 'react-redux'
import { View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Home from './src/screens/home/index'
import Login from './src/screens/login/index'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NativeBaseProvider, Center, Text } from 'native-base';
import Empty from './src/screens/empty';
import Logout from './src/screens/logout/index';
import Detail from './src/screens/detail/index';
// const Detail = React.lazy(() => import('./src/screens/detail/index'));
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
function HomeTabs() {
    const dispatch = useDispatch();
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName = route.name;
                    let name;
                    if (route.name === 'Trang chủ') {
                        name = 'Trang chủ';
                        iconName = 'home-outline';
                    } else if (route.name === 'Báo cáo') {
                        name = 'Báo cáo';
                        iconName = 'stats-chart-outline';
                    }
                    else if (route.name === 'Thông báo') {
                        name = 'Thông báo';
                        iconName = 'notifications-outline';
                    }
                    else if (route.name === 'Đăng xuất') {
                        name = 'Đăng xuất';
                        iconName = 'log-out-outline';
                    }
                    // You can return any component that you like here!
                    return <React.Fragment>
                        {focused === true ?
                            <View style={{
                                backgroundColor: '#1B91BF', padding: 5,
                                width: '70%', borderRadius: 10
                            }}>
                                <Center>
                                    <Ionicons name={iconName} size={size} color='#fff' />
                                </Center>
                            </View> :
                            <View>
                                <Ionicons name={iconName} size={size} color='#1B91BF' />
                            </View>
                        }
                    </React.Fragment>
                }, headerShown: false,
            })}
            tabBarOptions={{
                style: {
                    height: 70
                },
                labelStyle: {
                    color: '#000',
                    fontWeight: 'bold',
                    fontSize: 14,
                    paddingBottom: 5
                }
            }}
        >
            <Tab.Screen name="Trang chủ" component={Home} key={1} />
            <Tab.Screen name="Báo cáo" component={Empty} key={2} />
            <Tab.Screen name="Thông báo" component={Empty} key={3} />
            <Tab.Screen name="Đăng xuất" component={Empty} key={4}
                listeners={{
                    tabPress: (e) => {
                        e.preventDefault();
                        AsyncStorage.setItem('token', '');
                        AsyncStorage.setItem('name', '');
                        AsyncStorage.setItem('username', '');
                        AsyncStorage.setItem('title', '');
                        AsyncStorage.setItem('userid', '');
                        dispatch({
                            type: 'USER_SET_DATA', data: {
                                token: null,
                                username: null,
                                userid: null
                            }
                        });
                    },
                }}

            />
        </Tab.Navigator>
    );
}

export default function App() {
    const dispatch = useDispatch();
    const isToken = useSelector(state => state.token);
    const [isLogin, setisLogin] = useState(!(isToken === null || isToken === undefined || isToken === ''));
    useEffect(() => {
        if (isToken === null || isToken === undefined || isToken === '')
            setisLogin(false);
        else
            setisLogin(true);
    }, [isToken])
    useEffect(() => {
        AsyncStorage.getItem('token').then(result => {
            if (result !== null && result !== undefined)
                dispatch({
                    type: 'USER_SET_DATA', data: {
                        userid: 'ajgshdvkjhkj',
                        token: 'hjsdkhj'
                    }
                });
        })
    })
    return (
        <NavigationContainer >
            <NativeBaseProvider>
                <SafeAreaProvider>
                    {isLogin === false ?
                        <Stack.Navigator>
                            <Stack.Screen name="login" component={Login} options={{ headerShown: false }} />
                        </Stack.Navigator>
                        :
                        <Stack.Navigator>
                            <Stack.Screen name="Home" component={HomeTabs} options={{ headerShown: false }} />
                            <Stack.Screen name="Detail"
                                component={Detail}
                                options={{ headerShown: false }} />
                            <Stack.Screen name="Setting"
                                component={Empty}
                                options={{ headerShown: false }} />
                        </Stack.Navigator>
                    }
                </SafeAreaProvider>
            </NativeBaseProvider>
        </NavigationContainer>
    );
}
