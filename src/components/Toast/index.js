import { ToastAndroid } from 'react-native';
import PropTypes from 'prop-types';

export default function Toast({message}) {
    ToastAndroid.showWithGravityAndOffset(
        message,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
        25,
        50
    )
}

Toast.PropTypes = {
    message: PropTypes.string
}