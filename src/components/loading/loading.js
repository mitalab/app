import { Image, View, Center } from "native-base";
import { StyleSheet } from "react-native";
import React from 'react'

export default function Loading() {
    Image.res
    return (
        <Center>
            <View style={styles.container}>
                <Image size="xl" alt="loading" source={require('./loading-heart-rate.gif')} />
            </View>
        </Center>
    )
}
const styles = StyleSheet.create({
    container: {
        maxWidth: '100%',
    },
});