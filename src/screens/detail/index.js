import moment from 'moment';
import { Button, Center, Input, Select, Switch, Text, TextArea, View } from 'native-base';
import React, { useEffect, useState } from 'react';
import { Dimensions, Pressable, ScrollView, StyleSheet } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Ionicons';
export default function Detail({ route, navigation }) {
    const [date, setDate] = useState(new Date('2021-10-06'))
    const [height, setHeight] = useState(0);
    const [dateSelect, setDateSelect] = useState([]);
    const [monthSelect, setMonthSelect] = useState([]);
    const [yearSelect, setYearSelect] = useState([]);
    const [hour, setHour] = useState([]);
    const [minute, setMinute] = useState([]);
    const [switch1, setSwitch1] = useState(true);
    useEffect(() => {
        let { height } = Dimensions.get('window');
        setHeight(height);
        let dateV = [];
        let monV = [];
        let yearV = [];
        let hourV = [];
        let minV = [];
        for (let index = 1; index <= 31; index++) {
            dateV.push(index.toString())
            setDateSelect(dateV)
        }
        for (let index = 1980; index <= moment().year(); index++) {
            yearV.push(index.toString())
            setYearSelect(yearV)
        }
        for (let index = 1; index <= 12; index++) {
            monV.push(index.toString())
            setMonthSelect(monV)
        }
        for (let index = 0; index <= 59; index++) {
            minV.push(index.toString())
            setMinute(minV)
        }
        for (let index = 0; index <= 23; index++) {
            hourV.push(index.toString())
            setHour(hourV)
        }
    }, [])
    return (
        <SafeAreaView style={{ backgroundColor: '#fff', height: '100%' }}>
            <View style={{ marginLeft: 10, marginRight: 10, marginTop: 10 }}>
                <Pressable onPress={() => navigation.navigate('Home')}>
                    <Icon style={styles.icon} name="arrow-back-outline" size={30} color="#46B5BC" />
                </Pressable>
                <ScrollView style={{ height: height - 140 }}>
                    <View style={styles.row}>
                        <View style={styles.col}>
                            <Text style={styles.colText}>Report Number <Text style={styles.red}>*</Text></Text>
                            <Input style={styles.inputDisable} value="1610" isDisabled={true} />
                        </View>
                        <View style={styles.col}>
                            <Text style={styles.colText}>Mã phiên bản <Text style={styles.red}>*</Text></Text>
                            <Input style={styles.inputDisable} value="MT-2110-00018" isDisabled={true} />
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col}>
                            <Text style={styles.colText}>Model<Text style={styles.red}>*</Text></Text>
                            <Input style={styles.inputDisable} value="DxH500" isDisabled={true} />
                        </View>
                        <View style={styles.col}>
                            <Text style={styles.colText}>Serial <Text style={styles.red}>*</Text></Text>
                            <Input style={styles.inputDisable} value="Ay50966" isDisabled={true} />
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col}>
                            <Text style={styles.colText}>Mức dộ ưu tiên <Text style={styles.red}>*</Text></Text>
                            <Input style={styles.inputDisable} value="Bình thường" isDisabled={true} />
                        </View>
                        <View style={styles.col}>
                            <Text style={styles.colText}>Phân loại <Text style={styles.red}>*</Text></Text>
                            <Input style={styles.inputDisable} value="Bảo Dưỡng" isDisabled={true} />
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col12}>
                            <Text style={styles.colText}>Mã lỗi công ty <Text style={styles.red}>*</Text></Text>
                            <Select
                                selectedValue={'1'}
                                style={styles.select}
                            >
                                <Select.Item label="DXHCMS001 - Vent/overflow chamber detectd overflow" value="1" />
                                <Select.Item label="DXHCMS002 - Vent/overflow chamber drain did not sense empty" value="2" />
                                <Select.Item label="DXHCMS003 - Ambient temperature exceeded the operating limits" value="3" />
                                <Select.Item label="DXHCMS004 - Ambient temperature is approaching the operating limits" value="3" />
                            </Select>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col12}>
                            <Text style={styles.colText}>Mã lỗi hãng <Text style={styles.red}>*</Text></Text>
                            <Select
                                selectedValue={'1'}
                                style={styles.select}
                            >
                                <Select.Item label="DXHCMS001 - Vent/overflow chamber detectd overflow" value="1" />
                                <Select.Item label="DXHCMS002 - Vent/overflow chamber drain did not sense empty" value="2" />
                                <Select.Item label="DXHCMS003 - Ambient temperature exceeded the operating limits" value="3" />
                                <Select.Item label="DXHCMS004 - Ambient temperature is approaching the operating limits" value="3" />
                            </Select>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col12}>
                            <Text style={styles.colText}>Biểu hiện <Text style={styles.red}>*</Text></Text>
                            <TextArea
                                style={styles.Area}
                                placeholder="Enter your message"
                            />
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col12}>
                            <Text style={styles.colText}>Giải pháp <Text style={styles.red}>*</Text></Text>
                            <TextArea
                                style={styles.Area}
                                placeholder="Enter your message"
                            />
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col12}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '50%' }}>
                                    <Text style={styles.colText}>Thời gian bắt đầu đi <Text style={styles.red}>*</Text></Text>
                                </View>
                                <View style={{ width: '50%' }}>
                                    <Center>
                                        <Text style={styles.colText}>Giờ <Text style={styles.red}>*</Text></Text>
                                    </Center>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '20%' }}>
                                    <Select
                                        defaultValue={'15'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {dateSelect.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '20%' }}>
                                    <Select
                                        defaultValue={'4'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {monthSelect.map(value => <Select.Item label={`T${value}`} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '25%' }}>
                                    <Select
                                        defaultValue={`2021`}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {yearSelect.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '18%' }}>
                                    <Select
                                        defaultValue={'1'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {hour.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '18%' }}>
                                    <Select
                                        defaultValue={'0'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {minute.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col12}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '50%' }}>
                                    <Text style={styles.colText}>Bắt đầu <Text style={styles.red}>*</Text></Text>
                                </View>
                                <View style={{ width: '50%' }}>
                                    <Center>
                                        <Text style={styles.colText}>Giờ <Text style={styles.red}>*</Text></Text>
                                    </Center>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '20%' }}>
                                    <Select
                                        defaultValue={'15'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {dateSelect.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '20%' }}>
                                    <Select
                                        defaultValue={'4'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {monthSelect.map(value => <Select.Item label={`T${value}`} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '25%' }}>
                                    <Select
                                        defaultValue={`2021`}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {yearSelect.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '18%' }}>
                                    <Select
                                        defaultValue={'1'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {hour.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '18%' }}>
                                    <Select
                                        defaultValue={'0'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {minute.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col12}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '50%' }}>
                                    <Text style={styles.colText}>Kết thúc <Text style={styles.red}>*</Text></Text>
                                </View>
                                <View style={{ width: '50%' }}>
                                    <Center>
                                        <Text style={styles.colText}>Giờ <Text style={styles.red}>*</Text></Text>
                                    </Center>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '20%' }}>
                                    <Select
                                        defaultValue={'15'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {dateSelect.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '20%' }}>
                                    <Select
                                        defaultValue={'4'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {monthSelect.map(value => <Select.Item label={`T${value}`} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '25%' }}>
                                    <Select
                                        defaultValue={`2021`}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {yearSelect.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '18%' }}>
                                    <Select
                                        defaultValue={'1'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {hour.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                                <View style={{ width: '18%' }}>
                                    <Select
                                        defaultValue={'0'}
                                        style={[styles.selectDate, styles.col20]}
                                    >
                                        {minute.map(value => <Select.Item label={value} value={value} key={value} />)}
                                    </Select>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={[styles.col, { flexDirection: 'row' }]}>
                            <Text style={styles.colText}>Làm thêm trưa</Text>
                            <Switch size="md" style={{ marginLeft: 'auto' }}
                                isChecked={switch1}
                                onToggle={e => {
                                    setSwitch1(!switch1)
                                }
                                }
                            />
                        </View>
                        <View style={[styles.col, { flexDirection: 'row' }]}>
                            <Text style={styles.colText}>Đi làm</Text>
                            <Switch size="md" style={{ marginLeft: 'auto' }} />
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={[styles.col, { flexDirection: 'row' }]}>
                            <Text style={styles.colText}>Ngày nghỉ</Text>
                            <Switch size="md" style={{ marginLeft: 'auto' }} />
                        </View>
                        <View style={[styles.col, { flexDirection: 'row' }]}>
                            <Text style={[styles.colText]}>Ngày lễ</Text>
                            <Switch size="md" style={{ marginLeft: 'auto' }} />
                        </View>
                    </View>
                    <View style={styles.row}>
                        <View style={styles.col12}>
                            <Text style={styles.colText}>Đính kèm <Text style={styles.red}>*</Text></Text>
                            <TextArea
                                style={styles.Area}
                                placeholder="Upload photo"
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
            <View style={styles.footer}>
                <Center>
                    <Button
                        style={{
                            backgroundColor: '#1B91BF', paddingBottom: 15, paddingTop: 15,
                            height: 50, borderRadius: 15, width: '80%'
                        }} onPress={() => navigation.navigate('Home')}>
                        Hoàn thành
                    </Button>
                </Center>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    col: {
        width: '50%',
    },
    col20: {
        width: '25%',
    },
    col25: {
        width: '25%',
    },
    col12: {
        width: '95%',
    },
    colText: {
        fontSize: 16,
        color: '#8D8D8D',
        fontWeight: '700'
    },
    inputDisable: {
        fontSize: 16,
        fontWeight: '700',
        width: '90%',
        backgroundColor: '#FAFAFA'
    },
    Area: {
        fontSize: 16,
        fontWeight: '700',
        width: '100%',
        backgroundColor: '#FAFAFA',
        textAlignVertical: 'top'
    },
    select: {
        fontSize: 16,
        fontWeight: '700',
    },
    selectDate: {
        fontSize: 14,
        textAlign: 'right',
        paddingRight: 0
    },
    red: {
        fontSize: 16,
        color: '#FF814F',
        fontWeight: '700'
    },
    footer: {
        height: 40,
        paddingTop: 10,
        width: '100%',
        height: 70,
        flex: 1,
        position: 'absolute',
        bottom: 0,
    },
    textHeader: {
        fontWeight: 'bold',
        fontSize: 35,
        marginBottom: 10,
        marginTop: 10,
        lineHeight: 50
    },
    textForm: {
        color: '#3E2935',
        fontWeight: 'bold',
        fontSize: 22,
        marginBottom: 10,
        marginTop: 10,
        lineHeight: 30
    }
})