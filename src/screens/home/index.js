import { Icon as IconBase, Center, Text, View, Input, Modal, Button } from 'native-base';
import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, TouchableOpacity, BackHandler, StatusBar, ImageBackground, Pressable } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/Ionicons';
import Toast from '../../components/Toast';
import DatePicker from 'react-native-date-picker';

var countExit = 0;
export default ({ navigation }) => {
    const [date, setDate] = useState(new Date())
    const [open, setOpen] = useState(false)
    const [datasource, setDatasource] = useState([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
    const [show, setShow] = useState(false)
    const [search, setSearch] = useState('');
    const [clear, setClear] = useState(false);
    const [showSearch, setshowSearch] = useState(false)
    const backAction = () => {
        if (navigation.isFocused()) {
            Toast({ message: 'Nhấn lần nữa để thoát.' })
            countExit = countExit + 1;
            if (countExit === 2)
                BackHandler.exitApp();
            setTimeout(() => {
                countExit = 0;
            }, 2000);
            return true;
        }
    };

    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", backAction);

        return () =>
            BackHandler.removeEventListener("hardwareBackPress", backAction);
    }, []);
    return (
        <React.Fragment>
            <DatePicker
                title="Chọn ngày"
                cancelText="Hủy"
                confirmText="Đồng ý"
                modal
                mode='date'
                open={open}
                date={date}
                onConfirm={(date) => {
                    setOpen(false)
                    setDate(date)
                }}
                onCancel={() => {
                    setOpen(false)
                }}
            />
            <View style={{ height: 159 }}>
                <ImageBackground source={require('../../images/logo1.png')} style={styles.imgHeader} >
                    <View style={styles.header}>
                        <StatusBar barStyle="light-content" backgroundColor="transparent" translucent={true} />
                        <Text style={styles.textHeader}>
                            Service FAST
                        </Text>
                    </View>
                    <Center>
                        <View style={styles.search}>
                            <View style={styles.searchIcon}>
                                <Center>
                                    <TouchableOpacity
                                        onPress={() => setOpen(true)}
                                    >
                                        <Icon name="calendar-outline" size={40} color="#5A8DBC" />
                                    </TouchableOpacity>
                                </Center>
                            </View>
                            <View style={styles.searchIcon}>
                                <Center>
                                    <TouchableOpacity
                                        onPress={() => setShow(true)}
                                    >
                                        <Icon name="search-outline" size={40} color="#5A8DBC" />
                                    </TouchableOpacity>
                                </Center>
                            </View>
                        </View>
                    </Center>
                </ImageBackground>
            </View>
            <ScrollView>
                {datasource.map((row, index) => (
                    <View style={[styles.container, { backgroundColor: '#1B91BF' }]} key={index}>
                        <Pressable onPress={() => navigation.navigate('Detail', {
                        })}>
                            <View style={styles.row}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.textItem}>
                                        2021-10-16
                                    </Text>
                                    <Text style={styles.textItemRight}>Đa khoa khu vực Quãng Nam</Text>
                                </View>
                            </View>
                            <Center>
                                <Text style={{ paddingBottom: 10, color: '#fff' }}>DxH900 - BC08183</Text>
                            </Center>
                        </Pressable>
                    </View>
                ))}
            </ScrollView>
            <Modal isOpen={show} onClose={() => setShow(false)} style={{ width: '100%' }} >
                <Modal.Content>
                    <Modal.Body>
                        <View style={{
                            flexDirection: 'row', width: '100%', backgroundColor: '#fff', borderRadius: 10,
                        }}>
                            <Center>
                                <TouchableOpacity onPress={() => { }}>
                                    <IconBase
                                        as={<MaterialIcons name='search' />}
                                        size={5}
                                        mr="2"
                                        color="muted.400"
                                    />

                                </TouchableOpacity>
                            </Center>
                            <View style={{ width: '90%' }}>
                                <Input
                                    style={styles.inputSearch}
                                    placeholder="Search"
                                    value={search}
                                    onChangeText={e => setSearch(e)}
                                    // onFocus={e => { setClear(true) }}
                                    // onBlur={e => { setClear(false) }}
                                    InputRightElement={
                                        <React.Fragment>
                                            {search !== '' &&
                                                <Button onPress={() => { setSearch('') }}
                                                    style={{ backgroundColor: 'transparent' }}><IconBase
                                                        as={<MaterialIcons name='close' />}
                                                        size={5}
                                                        color="muted.400"
                                                    /></Button>
                                            }
                                        </React.Fragment>
                                    }
                                />
                            </View>
                        </View>
                    </Modal.Body>
                </Modal.Content>
            </Modal>
        </React.Fragment>
    );
}

const styles = StyleSheet.create({
    textHeader: {
        fontWeight: 'bold',
        fontSize: 32,
        marginBottom: 10,
        marginTop: 10,
        lineHeight: 50,
        color: '#fff'
    },
    container: {
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 7,
        marginBottom: 5,
        marginTop: 3,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },
    row: {
        flexDirection: "row",
        flexWrap: "wrap",
        width: "100%",
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
    },
    textItem: {
        fontSize: 15, color: '#fff'
    },
    textItemRight: {
        fontSize: 15, color: '#fff', marginLeft: 15
    },
    imgHeader: {
        height: 140
    },
    header: {
        paddingTop: 30,
        paddingLeft: 10,
        paddingRight: 10,
    },
    inputSearch: {
        borderColor: 'blue',
        fontSize: 18,
        height: 50,
        width: '78%',
    },
    search: {
        width: '85%',
        borderColor: '#000',
        flexDirection: 'row',
        borderColor: '#000',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 15,
        paddingTop: 7,
        paddingBottom: 7,
        backgroundColor: '#fff'
    },
    searchIcon: {
        width: '50%',
        textAlign: 'center'
    }
})