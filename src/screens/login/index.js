import { Button, Center, Icon, Input } from 'native-base';
import React, { useState } from 'react';
import { Dimensions, Image, ImageBackground, StatusBar, StyleSheet, Text, TouchableOpacity, View, Pressable } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useDispatch } from 'react-redux';

export default function LoginScreen() {
    const dispatch = useDispatch();
    const [userName, setuserName] = useState('');
    const [Password, setPassword] = useState('');
    const [modalVisible, setModalVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [textMsg, setTextMsg] = useState('');
    const [show, setShow] = useState(false);
    function Login(username, password) {
        dispatch({
            type: 'USER_SET_DATA', data: {
                token: 'esgrsdhgesth',
                userid: 'etfgdjnvfbr'
            }
        });
    }

    const checkDisableBtn = () => {
        if (userName.trim() !== '' && Password.trim() !== '')
            return false;
        return true;
    }
    return (
        <View style={{ height: '100%' }}>
            <ImageBackground source={require('../../images/banner.jpg')} style={styles.imgHeader} >
                <StatusBar barStyle="light-content" backgroundColor="transparent" translucent={true} />
                <Center>
                    <View style={styles.avatar}>
                        <Image source={require('../../images/avatar.png')}
                            style={{
                                width: 80,
                                height: 80,
                                resizeMode: 'stretch',
                            }}
                        />
                    </View>
                    <Text style={{
                        marginBottom: 20,
                        fontWeight: 'bold',
                        color: '#fff',
                        fontSize: 30
                    }}>Sign in</Text>
                </Center>
                <View style={{ flex: 1, position: 'relative', alignItems: 'center', marginTop: 30 }}>
                    {/* <TextInput placeholder="User name" style={styles.input} value={userName}
                        onChangeText={(e) => setuserName(e)}
                    /> */}
                    <Text style={{
                        marginBottom: 20,
                        fontWeight: 'bold',
                        textAlign: 'left',
                        width: '90%',
                        color: '#fff',
                        fontSize: 25
                    }}>NGUYEN MANH HUNG</Text>
                    <View style={{
                        flexDirection: 'row', width: '90%', marginTop: 15, backgroundColor: '#fff', borderRadius: 10,
                        paddingRight: 5
                    }}>
                        <Input
                            style={{
                                width: Dimensions.get('window').width * 0.9 - 30,
                                borderColor: 'transparent',
                                fontSize: 18,
                                height: 50
                            }}
                            type={show ? "text" : "password"}
                            placeholder="Password"
                        />
                        <Center>
                            <TouchableOpacity style={{
                                height: 40, width: 30, flex: 1,
                                alignContent: 'center', alignItems: 'center', alignSelf: 'center'
                            }}
                                onPress={() => {
                                    setShow(!show)
                                }
                                }>
                                <View style={{
                                    alignContent: 'center', alignItems: 'center', alignSelf: 'center',
                                    height: '100%'
                                }}>
                                    <Icon
                                        style={{ marginTop: 15 }}
                                        as={<MaterialIcons name={!show ? "visibility-off" : "visibility"} />}
                                        size={5}
                                        mr="2"
                                        color="muted.400"
                                    />
                                </View>
                            </TouchableOpacity>
                        </Center>
                    </View>
                    <View style={{ flexDirection: 'row', width: '90%', marginTop: 15 }}>
                        <Button style={{
                            backgroundColor: '#5A8DBC',
                            borderRadius: 10,
                            width: Dimensions.get('window').width * 0.9 - 48,
                        }}
                            isLoading={loading}
                            onPress={() => {
                                Login(userName, Password);
                            }}
                            // disabled={checkDisableBtn()} 
                        ><Text
                            style={{ fontSize: 20, color: '#fff' }}
                        >Sign in</Text></Button>
                        <Image style={{ tintColor: '#fff' }} source={require('../../images/faceid.png')} />
                    </View>

                </View>
                <View style={{ marginBottom: 50, alignSelf: 'center' }}>
                    <Image source={require('../../images/logo.png')}
                        style={{
                            width: 100,
                            height: 33.33,

                        }}
                    />
                </View>
            </ImageBackground>

        </View>
    );
}
const styles = StyleSheet.create({
    imgHeader: {
        height: '100%'
    },
    input: {
        borderBottomColor: '#d9d9d9',
        borderBottomWidth: 1,
        width: '90%',
        fontSize: 20,
        height: 50,
        marginBottom: 10,
        backgroundColor: '#fff',
        borderRadius: 8,
        padding: 10

    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    avatar: {
        marginBottom: 20, marginTop: 60, overflow: 'hidden', borderRadius: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }
});