
const unloadedState = {
    token: '',
    name: '',
    groupid: '',
    userid: '',
    username:'',
    title: '',
};

export default Reducers = (state, incomingAction) => {
    if (state === undefined) {
        return unloadedState;
    }
    const action = incomingAction
    switch (action.type) {
        case 'USER_SET_DATA':
            return action.data;
        default:
    }

    return state;
};